# Satracker

This Python script can calculate the azimuth and elevation of a satellite for a given ground station.

## Dependencies

The following are the current dependencies:

* [astropy](https://github.com/astropy/astropy)
* [ephem](https://github.com/brandon-rhodes/pyephem)
* [pyserial](https://github.com/pyserial/pyserial)
* [sgp4](https://github.com/brandon-rhodes/python-sgp4)

## Installation

```bash
pip3 install astropy
pip3 install ephem
pip3 install pyserial
pip3 install sgp4
```

## Usage

You need to tell the script which satellite you want to track, from what TLE file it can find it and wheter you want to update the TLE files or not (0 or 1).

Example 1:
We want to track the CubeSat SUCHAI-3, which can be found in the ''active'' category of Celestrak, and we want to update the TLE files:
```bash
python3 calc_az_el.py SUCHAI-3 active 1
```

Example 2:
What if we want to track the ISS (ZARYA), from the 'tdrss'' category, but we want to update the TLE files first?
```bash
python3 calc_az_el.py ISS \(ZARYA\) tdrss 1
```
