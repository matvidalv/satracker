from astropy.time import Time
from astropy.coordinates import AltAz, EarthLocation, ITRS, TEME, CartesianDifferential, CartesianRepresentation
from astropy import units as u
from datetime import datetime, timedelta
from sgp4.api import Satrec
from sgp4.api import SGP4_ERRORS
from time import sleep
from urllib.request import urlopen
import serial
import sys

def updateTLEfromNet():
    """
    Updates the TLE files from the celestrack web page.
    This function is based on the program Pypredict.
    """
    tle_files = ["active", "argos", "beidou", "cubesat", "dmc",
                 "education", "geodetic", "goes", "intelsat",
                 "iridium", "iridium-NEXT", "military",
                 "molniya", "noaa", "oneweb", "planet", "radar",
                 "resource", "sarsat", "spire", "starlink",
                 "tdrss", "tle-new", "visual", "weather", "x-comm"]
    for i, file_name in enumerate(tle_files):
        link = f"https://celestrak.com/NORAD/elements/gp.php?GROUP={file_name}&FORMAT=tle"
        with open("data/{}.txt".format(file_name), "w") as dest:
            print(f"[{i+1}/{len(tle_files)}] Downloading {file_name}...")
            response = urlopen(link)
            dest.write(response.read().decode("utf-8"))

def readTLE(sat_name, file_name):
    """
    Search for the satellite name inside the file_name. If found,
    returns the next two lines as the two line elements of this
    satellite. This fuinction is based on Pypredict.
    """
    with open(file_name, 'r') as f:
        for count, line in enumerate(f):
            if (line.strip() == sat_name):
                line1 = next(f).strip()
                line2 = next(f).strip()
                break
    return line1, line2

def init_serial():
    try:
        serial_con = serial.Serial(
                         port='/dev/ttyACM0',
                         baudrate=9600#,
                         #parity=serial.PARITY_ODD,
                         #stopbits=serial.STOPBITS_TWO,
                         #bytesize=serial.SEVENBITS
                     )
    except serial.SerialException as e:
        print(e)
        return None
    else:
        return serial_con

def lat_lon_alt(sat):
    t = Time.now()
    error_code, teme_p, teme_v = sat.sgp4(t.jd1, t.jd2)  # in km and km/s
    if error_code != 0:
        raise RuntimeError(SGP4_ERRORS[error_code])
    teme_p = CartesianRepresentation(teme_p*u.km)
    teme_v = CartesianDifferential(teme_v*u.km/u.s)
    teme = TEME(teme_p.with_differentials(teme_v), obstime=t)
    itrs_geo = teme.transform_to(ITRS(obstime=t))
    location = itrs_geo.earth_location
    return location.geodetic.lat.deg, location.geodetic.lon.deg, location.geodetic.height.value

def az_el_dist(latitude, longitude, altitude, ground):
    sat_loc = EarthLocation.from_geodetic(lat=latitude, lon=longitude, height=altitude)
    result = sat_loc.get_itrs().transform_to(AltAz(location=ground))
    return result.az.deg, result.alt.deg

def angles_to_str(azimuth, elevation):
    return f"AZ{azimuth:05.1f} EL{elevation:05.1f}"

def next_pass(sat, initial_time, step):
    date = initial_time
    AOS = initial_time#+timedelta(minutes=1)
    LOS = initial_time#+timedelta(minutes=1)
    while (AOS == initial_time):
        date += timedelta(seconds=step)
        sat.updateOrbitalParameters(date)
        sat_loc = EarthLocation.from_geodetic(lat=sat.getLat(), lon=sat.getLng(), height=sat.getAlt())
        result = sat_loc.get_itrs().transform_to(AltAz(location=ground))
        #print("{:12.2f}".format(result.distance))
        if (result.alt.deg > 0):
            AOS = date
    while (LOS == initial_time):
        date += timedelta(seconds=step)
        sat.updateOrbitalParameters(date)
        sat_loc = EarthLocation.from_geodetic(lat=sat.getLat(), lon=sat.getLng(), height=sat.getAlt())
        result = sat_loc.get_itrs().transform_to(AltAz(location=ground))
        if (result.alt.deg < 0):
            LOS = date
    return AOS, LOS

args = sys.argv

if (len(args) > 4):
    sat_name = f"{args[1]} {args[2]}"
    filename = f"{args[3]}"
    update = int(args[4])
else:
    sat_name = f"{args[1]}"
    filename = f"{args[2]}"
    update = int(args[3])

if (update):
    updateTLEfromNet()

serial_port = init_serial()
#ser.write("AZ180.0 EL090.0\r\n".encode())

ground  = EarthLocation.from_geodetic(lat=-33.45814, lon=-70.66198, height=550)
line1, line2 = readTLE(sat_name, f"data/{filename}.txt")
while (1):
    sat = Satrec.twoline2rv(line1, line2)
    lat, lon, alt = lat_lon_alt(sat)
    az, el = az_el_dist(lat, lon, alt, ground)
    cmd = angles_to_str(az, el)
    print(cmd)
    if (el > 0.0):
        serial_port.write(cmd.encode('utf-8'))
    sleep(1)

#AOS, LOS = next_pass(sat, datetime.utcnow(), 60*5)
#AOS, LOS = next_pass(sat, AOS - timedelta(minutes=5), 5)
#print("AOS: {}\nLOS: {}".format(AOS, LOS))
N = 120
for i in range(N):
    cmd = az_el_dist(sat)
    print("{:03}/{}: {}".format(i+1, N, cmd))
    ser.write(cmd.encode('utf-8'))
    sleep(5)

ser.close()
exit()
